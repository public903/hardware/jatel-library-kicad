PCBNEW-LibModule-V1  Čt 7. červen 2012, 19:13:35 CEST
# encoding utf-8
Units deci-mils
$INDEX
Conn_5x1VM10
conn_8x2F
$EndINDEX
$MODULE Conn_5x1VM10
Po 0 0 0 15 4FD0DE1C 00000000 ~~
Li Conn_5x1VM10
Cd Bornier d'alimentation 4 pins
Kw DEV
Sc 0
AR 
Op 0 0 0
T0 -200 -1900 393 393 0 78 N V 21 N "Conn_5x1VM10"
T1 0 -1100 393 393 0 78 N I 21 N "Value"
DS -5000 1500 5000 1500 120 21
DS -5000 1000 5000 1000 120 21
DS -5000 -1500 5000 -1500 120 21
DS 5000 -1500 5000 1500 120 21
DS -5000 -1500 -5000 1500 120 21
$PAD
Sh "2" C 1500 1500 0 0 0
Dr 600 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -2000 0
$EndPAD
$PAD
Sh "3" C 1500 1500 0 0 0
Dr 600 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 0 0
$EndPAD
$PAD
Sh "1" R 1500 1500 0 0 0
Dr 600 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -4000 0
$EndPAD
$PAD
Sh "4" C 1500 1500 0 0 0
Dr 600 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 2000 0
$EndPAD
$PAD
Sh "5" C 1500 1500 0 0 0
Dr 600 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 4000 0
$EndPAD
$SHAPE3D
Na "device/bornier_5.wrl"
Sc 1 1 1
Of 0 0 0
Ro 0 0 0
$EndSHAPE3D
$EndMODULE Conn_5x1VM10
$MODULE conn_8x2F
Po 0 0 0 15 4FD0E11F 00000000 ~~
Li conn_8x2F
Sc 0
AR 
Op 0 0 0
T0 0 -1600 393 393 0 78 N V 21 N "conn_8x2F"
T1 0 2000 393 393 0 78 N I 21 N "Value"
DS 4200 1500 -4200 1500 100 21
DS -4200 1200 4200 1200 100 21
DS 4000 1000 -4000 1000 100 21
DS 4000 -1000 -4000 -1000 100 21
DS 4200 -1200 -4200 -1200 100 21
DS 4200 1200 4200 1500 100 21
DS -4200 1500 -4200 1200 100 21
DS 4200 1200 4200 -1200 100 21
DS 4000 -1000 4000 1000 100 21
DS -4200 1200 -4200 -1200 100 21
DS -4000 1000 -4000 -1000 100 21
$PAD
Sh "1" R 750 750 0 0 0
Dr 393 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -3500 -500
$EndPAD
$PAD
Sh "2" C 750 750 0 0 0
Dr 393 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -3500 500
$EndPAD
$PAD
Sh "3" C 750 750 0 0 0
Dr 393 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -2500 -500
$EndPAD
$PAD
Sh "4" C 750 750 0 0 0
Dr 393 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -2500 500
$EndPAD
$PAD
Sh "5" C 750 750 0 0 0
Dr 393 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -1500 -500
$EndPAD
$PAD
Sh "6" C 750 750 0 0 0
Dr 393 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -1500 500
$EndPAD
$PAD
Sh "7" C 750 750 0 0 0
Dr 393 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -500 -500
$EndPAD
$PAD
Sh "8" C 750 750 0 0 0
Dr 393 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -500 500
$EndPAD
$PAD
Sh "9" C 750 750 0 0 0
Dr 393 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 500 -500
$EndPAD
$PAD
Sh "10" C 750 750 0 0 0
Dr 393 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 500 500
$EndPAD
$PAD
Sh "11" C 750 750 0 0 0
Dr 393 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 1500 -500
$EndPAD
$PAD
Sh "12" C 750 750 0 0 0
Dr 393 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 1500 500
$EndPAD
$PAD
Sh "13" C 750 750 0 0 0
Dr 393 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 2500 -500
$EndPAD
$PAD
Sh "14" C 750 750 0 0 0
Dr 393 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 2500 500
$EndPAD
$PAD
Sh "15" C 750 750 0 0 0
Dr 393 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 3500 -500
$EndPAD
$PAD
Sh "16" C 750 750 0 0 0
Dr 393 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 3500 500
$EndPAD
$EndMODULE conn_8x2F
$EndLIBRARY
