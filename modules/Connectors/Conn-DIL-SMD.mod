PCBNEW-LibModule-V1  Čt 21. červen 2012, 21:29:20 CEST
# encoding utf-8
Units deci-mils
$INDEX
10PIN_DIL_SMD_2.54MM
16PIN_DIL_SMD_2.54MM
20PIN_DIL_SMD_2.54MM
40PIN_DIL_SMD_2.54MM
$EndINDEX
$MODULE 10PIN_DIL_SMD_2.54MM
Po 0 0 0 15 4FE36FCB 00000000 ~~
Li 10PIN_DIL_SMD_2.54MM
Sc 0
AR 
Op 0 0 0
T0 0 -2500 472 472 0 100 N V 21 N "10PIN_DIL_SMD_2.54MM"
T1 0 2500 472 472 0 100 N I 21 N "VAL**"
DS 2500 -2000 2500 2000 150 21
DS 2500 -2000 -2500 -2000 150 21
DS -2500 -2000 -2500 2000 150 21
DS -2500 2000 2500 2000 150 21
DS -2900 1500 -2700 1500 100 21
DS -2900 1300 -2800 1100 100 21
DS -2800 1100 -2800 1500 100 21
$PAD
Sh "1" R 500 1000 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -2000 1075
$EndPAD
$PAD
Sh "2" R 500 1000 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -2000 -1075
$EndPAD
$PAD
Sh "3" R 500 1000 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -1000 1075
$EndPAD
$PAD
Sh "4" R 500 1000 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -1000 -1075
$EndPAD
$PAD
Sh "5" R 500 1000 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0 1075
$EndPAD
$PAD
Sh "6" R 500 1000 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0 -1075
$EndPAD
$PAD
Sh "7" R 500 1000 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 1000 1075
$EndPAD
$PAD
Sh "8" R 500 1000 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 1000 -1075
$EndPAD
$PAD
Sh "9" R 500 1000 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 2000 1075
$EndPAD
$PAD
Sh "10" R 500 1000 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 2000 -1075
$EndPAD
$EndMODULE 10PIN_DIL_SMD_2.54MM
$MODULE 16PIN_DIL_SMD_2.54MM
Po 0 0 0 15 4FE37057 00000000 ~~
Li 16PIN_DIL_SMD_2.54MM
Sc 0
AR 
Op 0 0 0
T0 0 -2500 472 472 0 100 N V 21 N "16PIN_DIL_SMD_2.54MM"
T1 0 2500 472 472 0 100 N I 21 N "VAL**"
DS -4000 2000 4000 2000 150 21
DS -4000 2000 -4000 -2000 150 21
DS -4000 -2000 4000 -2000 150 21
DS 4000 -2000 4000 2000 150 21
DS -4400 1500 -4200 1500 100 21
DS -4400 1300 -4300 1100 100 21
DS -4300 1100 -4300 1500 100 21
$PAD
Sh "1" R 500 1000 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -3500 1075
$EndPAD
$PAD
Sh "2" R 500 1000 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -3500 -1075
$EndPAD
$PAD
Sh "3" R 500 1000 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -2500 1075
$EndPAD
$PAD
Sh "4" R 500 1000 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -2500 -1075
$EndPAD
$PAD
Sh "5" R 500 1000 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -1500 1075
$EndPAD
$PAD
Sh "6" R 500 1000 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -1500 -1075
$EndPAD
$PAD
Sh "7" R 500 1000 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -500 1075
$EndPAD
$PAD
Sh "8" R 500 1000 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -500 -1075
$EndPAD
$PAD
Sh "9" R 500 1000 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 500 1075
$EndPAD
$PAD
Sh "10" R 500 1000 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 500 -1075
$EndPAD
$PAD
Sh "11" R 500 1000 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 1500 1075
$EndPAD
$PAD
Sh "12" R 500 1000 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 1500 -1075
$EndPAD
$PAD
Sh "13" R 500 1000 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 2500 1075
$EndPAD
$PAD
Sh "14" R 500 1000 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 2500 -1075
$EndPAD
$PAD
Sh "15" R 500 1000 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 3500 1075
$EndPAD
$PAD
Sh "16" R 500 1000 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 3500 -1075
$EndPAD
$EndMODULE 16PIN_DIL_SMD_2.54MM
$MODULE 20PIN_DIL_SMD_2.54MM
Po 0 0 0 15 4FE3709C 00000000 ~~
Li 20PIN_DIL_SMD_2.54MM
Sc 0
AR 
Op 0 0 0
T0 0 -2500 472 472 0 100 N V 21 N "20PIN_DIL_SMD_2.54MM"
T1 0 2500 472 472 0 100 N I 21 N "VAL**"
DS -5000 -2000 -5000 2000 150 21
DS -5000 2000 5000 2000 150 21
DS 5000 2000 5000 -2000 150 21
DS 5000 -2000 -5000 -2000 150 21
DS -5400 1600 -5200 1600 100 21
DS -5400 1400 -5300 1200 100 21
DS -5300 1200 -5300 1600 100 21
$PAD
Sh "1" R 500 1000 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -4500 1075
$EndPAD
$PAD
Sh "2" R 500 1000 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -4500 -1075
$EndPAD
$PAD
Sh "3" R 500 1000 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -3500 1075
$EndPAD
$PAD
Sh "4" R 500 1000 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -3500 -1075
$EndPAD
$PAD
Sh "5" R 500 1000 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -2500 1075
$EndPAD
$PAD
Sh "6" R 500 1000 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -2500 -1075
$EndPAD
$PAD
Sh "7" R 500 1000 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -1500 1075
$EndPAD
$PAD
Sh "8" R 500 1000 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -1500 -1075
$EndPAD
$PAD
Sh "9" R 500 1000 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -500 1075
$EndPAD
$PAD
Sh "10" R 500 1000 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -500 -1075
$EndPAD
$PAD
Sh "11" R 500 1000 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 500 1075
$EndPAD
$PAD
Sh "12" R 500 1000 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 500 -1075
$EndPAD
$PAD
Sh "13" R 500 1000 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 1500 1075
$EndPAD
$PAD
Sh "14" R 500 1000 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 1500 -1075
$EndPAD
$PAD
Sh "15" R 500 1000 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 2500 1075
$EndPAD
$PAD
Sh "16" R 500 1000 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 2500 -1075
$EndPAD
$PAD
Sh "17" R 500 1000 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 3500 1075
$EndPAD
$PAD
Sh "18" R 500 1000 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 3500 -1075
$EndPAD
$PAD
Sh "19" R 500 1000 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 4500 1075
$EndPAD
$PAD
Sh "20" R 500 1000 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 4500 -1075
$EndPAD
$EndMODULE 20PIN_DIL_SMD_2.54MM
$MODULE 40PIN_DIL_SMD_2.54MM
Po 0 0 0 15 4A8EC2F5 00000000 ~~
Li 40PIN_DIL_SMD_2.54MM
Sc 0
AR 
Op 0 0 0
T0 0 -2500 472 472 0 100 N V 21 N "40PIN_DIL_SMD_2.54MM"
T1 0 -3500 472 472 0 100 N I 21 N "VAL**"
DS -10400 1600 -10200 1600 100 21
DS -10400 1400 -10300 1200 100 21
DS -10300 1200 -10300 1600 100 21
DS -10400 1600 -10200 1600 100 27
DS -10400 1400 -10300 1200 100 27
DS -10300 1200 -10300 1600 100 27
DS -500 2000 500 2000 100 21
DS -500 -2000 500 -2000 100 21
DS -9000 -2000 -10000 -2000 100 21
DS -10000 -2000 -10000 -1000 100 21
DS 10000 -1000 10000 -2000 100 21
DS 10000 -2000 9000 -2000 100 21
DS 9000 2000 10000 2000 100 21
DS 10000 2000 10000 1000 100 21
DS -10000 1000 -10000 2000 100 21
DS -10000 2000 -9000 2000 100 21
DS -10000 2000 10000 2000 100 26
DS 10000 2000 10000 -2000 100 26
DS 10000 -2000 -10000 -2000 100 26
DS -10000 -2000 -10000 2000 100 26
DS -10000 -2000 10000 -2000 100 27
DS 10000 -2000 10000 2000 100 27
DS 10000 2000 -10000 2000 100 27
DS -10000 -2000 -10000 2000 100 27
$PAD
Sh "1" R 500 1000 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -9500 1075
$EndPAD
$PAD
Sh "2" R 500 1000 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -9500 -1075
$EndPAD
$PAD
Sh "3" R 500 1000 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -8500 1075
$EndPAD
$PAD
Sh "4" R 500 1000 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -8500 -1075
$EndPAD
$PAD
Sh "5" R 500 1000 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -7500 1075
$EndPAD
$PAD
Sh "6" R 500 1000 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -7500 -1075
$EndPAD
$PAD
Sh "7" R 500 1000 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -6500 1075
$EndPAD
$PAD
Sh "8" R 500 1000 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -6500 -1075
$EndPAD
$PAD
Sh "9" R 500 1000 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -5500 1075
$EndPAD
$PAD
Sh "10" R 500 1000 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -5500 -1075
$EndPAD
$PAD
Sh "11" R 500 1000 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -4500 1075
$EndPAD
$PAD
Sh "12" R 500 1000 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -4500 -1075
$EndPAD
$PAD
Sh "13" R 500 1000 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -3500 1075
$EndPAD
$PAD
Sh "14" R 500 1000 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -3500 -1075
$EndPAD
$PAD
Sh "15" R 500 1000 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -2500 1075
$EndPAD
$PAD
Sh "16" R 500 1000 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -2500 -1075
$EndPAD
$PAD
Sh "17" R 500 1000 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -1500 1075
$EndPAD
$PAD
Sh "18" R 500 1000 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -1500 -1075
$EndPAD
$PAD
Sh "19" R 500 1000 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -500 1075
$EndPAD
$PAD
Sh "20" R 500 1000 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -500 -1075
$EndPAD
$PAD
Sh "21" R 500 1000 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 500 1075
$EndPAD
$PAD
Sh "22" R 500 1000 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 500 -1075
$EndPAD
$PAD
Sh "23" R 500 1000 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 1500 1075
$EndPAD
$PAD
Sh "24" R 500 1000 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 1500 -1075
$EndPAD
$PAD
Sh "25" R 500 1000 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 2500 1075
$EndPAD
$PAD
Sh "26" R 500 1000 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 2500 -1075
$EndPAD
$PAD
Sh "27" R 500 1000 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 3500 1075
$EndPAD
$PAD
Sh "28" R 500 1000 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 3500 -1075
$EndPAD
$PAD
Sh "29" R 500 1000 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 4500 1075
$EndPAD
$PAD
Sh "30" R 500 1000 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 4500 -1075
$EndPAD
$PAD
Sh "31" R 500 1000 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 5500 1075
$EndPAD
$PAD
Sh "32" R 500 1000 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 5500 -1075
$EndPAD
$PAD
Sh "33" R 500 1000 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 6500 1075
$EndPAD
$PAD
Sh "34" R 500 1000 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 6500 -1075
$EndPAD
$PAD
Sh "35" R 500 1000 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 7500 1075
$EndPAD
$PAD
Sh "36" R 500 1000 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 7500 -1075
$EndPAD
$PAD
Sh "37" R 500 1000 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 8500 1075
$EndPAD
$PAD
Sh "38" R 500 1000 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 8500 -1075
$EndPAD
$PAD
Sh "39" R 500 1000 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 9500 1075
$EndPAD
$PAD
Sh "40" R 500 1000 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 9500 -1075
$EndPAD
$EndMODULE 40PIN_DIL_SMD_2.54MM
$EndLIBRARY
