PCBNEW-LibModule-V1  Čt 21. červen 2012, 21:27:49 CEST
# encoding utf-8
Units deci-mils
$INDEX
BUSPC_ISA62
DIMM_100
$EndINDEX
$MODULE BUSPC_ISA62
Po 0 0 0 15 4FE1F5D0 00000000 ~~
Li BUSPC_ISA62
Cd Conector PC ISA 62 pinos
Kw CONN DIN
Sc 0
AR 
Op 0 0 0
T0 0 -2500 393 393 0 78 N V 21 N "BUSPC_ISA62"
T1 0 2500 393 393 0 78 N I 21 N "Value"
DS -16600 1800 17200 1800 120 21
DS 17200 1800 17200 -2000 120 21
DS 17200 -2000 17000 -2000 120 21
DS -16600 -2000 -16600 1800 120 21
DS 16800 1700 17000 1700 120 21
DS 17100 -2000 16300 -2000 120 21
DS -15500 -2000 -16600 -2000 120 21
DS -16500 1700 -16500 1800 120 21
DS -15500 -2000 16600 -2000 120 21
$PAD
Sh "A1" C 600 600 0 0 0
Dr 320 0 0
At STD N 00E0FFFF
Ne 1 "A0_1"
Po -14500 1000
$EndPAD
$PAD
Sh "A2" C 600 600 0 0 0
Dr 320 0 0
At STD N 00E0FFFF
Ne 2 "A1_1"
Po -13500 1000
$EndPAD
$PAD
Sh "A3" C 600 600 0 0 0
Dr 320 0 0
At STD N 00E0FFFF
Ne 3 "A2_1"
Po -12500 1000
$EndPAD
$PAD
Sh "A4" C 600 600 0 0 0
Dr 320 0 0
At STD N 00E0FFFF
Ne 4 "A3_1"
Po -11500 1000
$EndPAD
$PAD
Sh "A5" C 600 600 0 0 0
Dr 320 0 0
At STD N 00E0FFFF
Ne 5 "A4_1"
Po -10500 1000
$EndPAD
$PAD
Sh "A6" C 600 600 0 0 0
Dr 320 0 0
At STD N 00E0FFFF
Ne 6 "A5_1"
Po -9500 1000
$EndPAD
$PAD
Sh "A7" C 600 600 0 0 0
Dr 320 0 0
At STD N 00E0FFFF
Ne 14 "B7_1"
Po -8500 1000
$EndPAD
$PAD
Sh "A8" C 600 600 0 0 0
Dr 320 0 0
At STD N 00E0FFFF
Ne 13 "B6_1"
Po -7500 1000
$EndPAD
$PAD
Sh "A9" C 600 600 0 0 0
Dr 320 0 0
At STD N 00E0FFFF
Ne 12 "B5_1"
Po -6500 1000
$EndPAD
$PAD
Sh "A10" C 600 600 0 0 0
Dr 320 0 0
At STD N 00E0FFFF
Ne 11 "B4_1"
Po -5500 1000
$EndPAD
$PAD
Sh "A11" C 600 600 0 0 0
Dr 320 0 0
At STD N 00E0FFFF
Ne 10 "B3_1"
Po -4500 1000
$EndPAD
$PAD
Sh "A12" C 600 600 0 0 0
Dr 320 0 0
At STD N 00E0FFFF
Ne 9 "B2_1"
Po -3500 1000
$EndPAD
$PAD
Sh "A13" C 600 600 0 0 0
Dr 320 0 0
At STD N 00E0FFFF
Ne 8 "B1_1"
Po -2500 1000
$EndPAD
$PAD
Sh "A14" C 600 600 0 0 0
Dr 320 0 0
At STD N 00E0FFFF
Ne 7 "B0_1"
Po -1500 1000
$EndPAD
$PAD
Sh "A15" C 600 600 0 0 0
Dr 320 0 0
At STD N 00E0FFFF
Ne 29 "VD_1"
Po -500 1000
$EndPAD
$PAD
Sh "A16" C 600 600 0 0 0
Dr 320 0 0
At STD N 00E0FFFF
Ne 30 "VS_1"
Po 500 1000
$EndPAD
$PAD
Sh "A17" C 600 600 0 0 0
Dr 320 0 0
At STD N 00E0FFFF
Ne 28 "D7_1"
Po 1500 1000
$EndPAD
$PAD
Sh "A18" C 600 600 0 0 0
Dr 320 0 0
At STD N 00E0FFFF
Ne 27 "D6_1"
Po 2500 1000
$EndPAD
$PAD
Sh "A19" C 600 600 0 0 0
Dr 320 0 0
At STD N 00E0FFFF
Ne 26 "D5_1"
Po 3500 1000
$EndPAD
$PAD
Sh "A20" C 600 600 0 0 0
Dr 320 0 0
At STD N 00E0FFFF
Ne 25 "D4_1"
Po 4500 1000
$EndPAD
$PAD
Sh "A21" C 600 600 0 0 0
Dr 320 0 0
At STD N 00E0FFFF
Ne 24 "D3_1"
Po 5500 1000
$EndPAD
$PAD
Sh "A22" C 600 600 0 0 0
Dr 320 0 0
At STD N 00E0FFFF
Ne 23 "D2_1"
Po 6500 1000
$EndPAD
$PAD
Sh "A23" C 600 600 0 0 0
Dr 320 0 0
At STD N 00E0FFFF
Ne 22 "D1_1"
Po 7500 1000
$EndPAD
$PAD
Sh "A24" C 600 600 0 0 0
Dr 320 0 0
At STD N 00E0FFFF
Ne 21 "D0_1"
Po 8500 1000
$EndPAD
$PAD
Sh "A25" C 600 600 0 0 0
Dr 320 0 0
At STD N 00E0FFFF
Ne 20 "C5_1"
Po 9500 1000
$EndPAD
$PAD
Sh "A26" C 600 600 0 0 0
Dr 320 0 0
At STD N 00E0FFFF
Ne 19 "C4_1"
Po 10500 1000
$EndPAD
$PAD
Sh "A27" C 600 600 0 0 0
Dr 320 0 0
At STD N 00E0FFFF
Ne 18 "C3_1"
Po 11500 1000
$EndPAD
$PAD
Sh "A28" C 600 600 0 0 0
Dr 320 0 0
At STD N 00E0FFFF
Ne 17 "C2_1"
Po 12500 1000
$EndPAD
$PAD
Sh "A29" C 600 600 0 0 0
Dr 320 0 0
At STD N 00E0FFFF
Ne 16 "C1_1"
Po 13500 1000
$EndPAD
$PAD
Sh "A30" C 600 600 0 0 0
Dr 320 0 0
At STD N 00E0FFFF
Ne 15 "C0_1"
Po 14500 1000
$EndPAD
$PAD
Sh "A31" C 600 600 0 0 0
Dr 320 0 0
At STD N 00E0FFFF
Ne 31 "Ve_1"
Po 15500 1000
$EndPAD
$PAD
Sh "B1" C 600 600 0 0 0
Dr 320 0 0
At STD N 00E0FFFF
Ne 1 "A0_1"
Po -14500 -1000
$EndPAD
$PAD
Sh "B2" C 600 600 0 0 0
Dr 320 0 0
At STD N 00E0FFFF
Ne 2 "A1_1"
Po -13500 -1000
$EndPAD
$PAD
Sh "B3" C 600 600 0 0 0
Dr 320 0 0
At STD N 00E0FFFF
Ne 3 "A2_1"
Po -12500 -1000
$EndPAD
$PAD
Sh "B4" C 600 600 0 0 0
Dr 320 0 0
At STD N 00E0FFFF
Ne 4 "A3_1"
Po -11500 -1000
$EndPAD
$PAD
Sh "B5" C 600 600 0 0 0
Dr 320 0 0
At STD N 00E0FFFF
Ne 5 "A4_1"
Po -10500 -1000
$EndPAD
$PAD
Sh "B6" C 600 600 0 0 0
Dr 320 0 0
At STD N 00E0FFFF
Ne 6 "A5_1"
Po -9500 -1000
$EndPAD
$PAD
Sh "B7" C 600 600 0 0 0
Dr 320 0 0
At STD N 00E0FFFF
Ne 14 "B7_1"
Po -8500 -1000
$EndPAD
$PAD
Sh "B8" C 600 600 0 0 0
Dr 320 0 0
At STD N 00E0FFFF
Ne 13 "B6_1"
Po -7500 -1000
$EndPAD
$PAD
Sh "B9" C 600 600 0 0 0
Dr 320 0 0
At STD N 00E0FFFF
Ne 12 "B5_1"
Po -6500 -1000
$EndPAD
$PAD
Sh "B10" C 600 600 0 0 0
Dr 320 0 0
At STD N 00E0FFFF
Ne 11 "B4_1"
Po -5500 -1000
$EndPAD
$PAD
Sh "B11" C 600 600 0 0 0
Dr 320 0 0
At STD N 00E0FFFF
Ne 10 "B3_1"
Po -4500 -1000
$EndPAD
$PAD
Sh "B12" C 600 600 0 0 0
Dr 320 0 0
At STD N 00E0FFFF
Ne 9 "B2_1"
Po -3500 -1000
$EndPAD
$PAD
Sh "B13" C 600 600 0 0 0
Dr 320 0 0
At STD N 00E0FFFF
Ne 8 "B1_1"
Po -2500 -1000
$EndPAD
$PAD
Sh "B14" C 600 600 0 0 0
Dr 320 0 0
At STD N 00E0FFFF
Ne 7 "B0_1"
Po -1500 -1000
$EndPAD
$PAD
Sh "B15" C 600 600 0 0 0
Dr 320 0 0
At STD N 00E0FFFF
Ne 29 "VD_1"
Po -500 -1000
$EndPAD
$PAD
Sh "B16" C 600 600 0 0 0
Dr 320 0 0
At STD N 00E0FFFF
Ne 30 "VS_1"
Po 500 -1000
$EndPAD
$PAD
Sh "B17" C 600 600 0 0 0
Dr 320 0 0
At STD N 00E0FFFF
Ne 28 "D7_1"
Po 1500 -1000
$EndPAD
$PAD
Sh "B18" C 600 600 0 0 0
Dr 320 0 0
At STD N 00E0FFFF
Ne 27 "D6_1"
Po 2500 -1000
$EndPAD
$PAD
Sh "B19" C 600 600 0 0 0
Dr 320 0 0
At STD N 00E0FFFF
Ne 26 "D5_1"
Po 3500 -1000
$EndPAD
$PAD
Sh "B20" C 600 600 0 0 0
Dr 320 0 0
At STD N 00E0FFFF
Ne 25 "D4_1"
Po 4500 -1000
$EndPAD
$PAD
Sh "B21" C 600 600 0 0 0
Dr 320 0 0
At STD N 00E0FFFF
Ne 24 "D3_1"
Po 5500 -1000
$EndPAD
$PAD
Sh "B22" C 600 600 0 0 0
Dr 320 0 0
At STD N 00E0FFFF
Ne 23 "D2_1"
Po 6500 -1000
$EndPAD
$PAD
Sh "B23" C 600 600 0 0 0
Dr 320 0 0
At STD N 00E0FFFF
Ne 22 "D1_1"
Po 7500 -1000
$EndPAD
$PAD
Sh "B24" C 600 600 0 0 0
Dr 320 0 0
At STD N 00E0FFFF
Ne 21 "D0_1"
Po 8500 -1000
$EndPAD
$PAD
Sh "B25" C 600 600 0 0 0
Dr 320 0 0
At STD N 00E0FFFF
Ne 20 "C5_1"
Po 9500 -1000
$EndPAD
$PAD
Sh "B26" C 600 600 0 0 0
Dr 320 0 0
At STD N 00E0FFFF
Ne 19 "C4_1"
Po 10500 -1000
$EndPAD
$PAD
Sh "B27" C 600 600 0 0 0
Dr 320 0 0
At STD N 00E0FFFF
Ne 18 "C3_1"
Po 11500 -1000
$EndPAD
$PAD
Sh "B28" C 600 600 0 0 0
Dr 320 0 0
At STD N 00E0FFFF
Ne 17 "C2_1"
Po 12500 -1000
$EndPAD
$PAD
Sh "B29" C 600 600 0 0 0
Dr 320 0 0
At STD N 00E0FFFF
Ne 16 "C1_1"
Po 13500 -1000
$EndPAD
$PAD
Sh "B30" C 600 600 0 0 0
Dr 320 0 0
At STD N 00E0FFFF
Ne 15 "C0_1"
Po 14500 -1000
$EndPAD
$PAD
Sh "B31" C 600 600 0 0 0
Dr 320 0 0
At STD N 00E0FFFF
Ne 31 "Ve_1"
Po 15500 -1000
$EndPAD
$SHAPE3D
Na "connectors/c62ISA.wrl"
Sc 1 1 1
Of 0 0 0
Ro 0 0 0
$EndSHAPE3D
$EndMODULE BUSPC_ISA62
$MODULE DIMM_100
Po 0 0 0 15 4FE37588 00000000 ~~
Li DIMM_100
Sc 0
AR 
Op 0 0 0
T0 0 -2087 472 472 0 100 N V 21 N "DIMM_100"
T1 -39 -2756 472 472 0 100 N I 21 N "VAL**"
DS -24094 1614 24134 1614 150 26
DS 24134 1614 24134 -1614 150 26
DS 24134 -1614 -24094 -1614 150 26
DS -24094 -1614 -24094 1614 150 26
DS -24094 1614 24134 1614 150 21
DS 24134 1614 24134 -1614 150 21
DS 24134 -1614 -24094 -1614 150 21
DS -24094 -1614 -24094 1614 150 21
DS -9764 -1457 -9764 1417 150 27
DS -11299 -1417 -11299 1457 150 27
DS 315 -1457 315 1457 150 27
DS -1220 -1457 -1220 1457 150 27
DS 20591 -1457 20591 1457 150 27
DS -20591 -1457 -20591 1457 150 27
DS -23937 1457 23937 1457 150 27
DS 23937 1457 23937 -1457 150 27
DS 23937 -1457 -23937 -1457 150 27
DS -23937 -1457 -23937 1457 150 27
$PAD
Sh "1" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po -14250 376
$EndPAD
$PAD
Sh "2" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po -13750 1126
$EndPAD
$PAD
Sh "3" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po -13250 376
$EndPAD
$PAD
Sh "4" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po -12750 1126
$EndPAD
$PAD
Sh "5" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po -12250 376
$EndPAD
$PAD
Sh "6" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po -11750 1126
$EndPAD
$PAD
Sh "7" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po -9250 376
$EndPAD
$PAD
Sh "8" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po -8750 1126
$EndPAD
$PAD
Sh "9" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po -8250 376
$EndPAD
$PAD
Sh "10" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po -7750 1126
$EndPAD
$PAD
Sh "11" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po -7250 376
$EndPAD
$PAD
Sh "12" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po -6750 1126
$EndPAD
$PAD
Sh "13" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po -6250 376
$EndPAD
$PAD
Sh "14" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po -5750 1126
$EndPAD
$PAD
Sh "15" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po -5250 376
$EndPAD
$PAD
Sh "16" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po -4750 1126
$EndPAD
$PAD
Sh "17" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po -4250 376
$EndPAD
$PAD
Sh "18" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po -3750 1126
$EndPAD
$PAD
Sh "19" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po -3250 376
$EndPAD
$PAD
Sh "20" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po -2750 1126
$EndPAD
$PAD
Sh "21" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po -2250 376
$EndPAD
$PAD
Sh "22" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po -1750 1126
$EndPAD
$PAD
Sh "23" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po 750 376
$EndPAD
$PAD
Sh "24" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po 1250 1126
$EndPAD
$PAD
Sh "25" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po 1750 376
$EndPAD
$PAD
Sh "26" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po 2250 1126
$EndPAD
$PAD
Sh "27" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po 2750 376
$EndPAD
$PAD
Sh "28" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po 3250 1126
$EndPAD
$PAD
Sh "29" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po 3750 376
$EndPAD
$PAD
Sh "30" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po 4250 1126
$EndPAD
$PAD
Sh "31" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po 4750 376
$EndPAD
$PAD
Sh "32" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po 5250 1126
$EndPAD
$PAD
Sh "33" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po 5750 376
$EndPAD
$PAD
Sh "34" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po 6250 1126
$EndPAD
$PAD
Sh "35" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po 6750 376
$EndPAD
$PAD
Sh "36" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po 7250 1126
$EndPAD
$PAD
Sh "37" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po 7750 376
$EndPAD
$PAD
Sh "38" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po 8250 1126
$EndPAD
$PAD
Sh "39" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po 8750 376
$EndPAD
$PAD
Sh "40" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po 9250 1126
$EndPAD
$PAD
Sh "41" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po 9750 376
$EndPAD
$PAD
Sh "42" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po 10250 1126
$EndPAD
$PAD
Sh "43" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po 10750 376
$EndPAD
$PAD
Sh "44" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po 11250 1126
$EndPAD
$PAD
Sh "45" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po 11750 376
$EndPAD
$PAD
Sh "46" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po 12250 1126
$EndPAD
$PAD
Sh "47" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po 12750 376
$EndPAD
$PAD
Sh "48" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po 13250 1126
$EndPAD
$PAD
Sh "49" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po 13750 376
$EndPAD
$PAD
Sh "50" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po 14250 1126
$EndPAD
$PAD
Sh "51" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po -14250 -1126
$EndPAD
$PAD
Sh "52" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po -13750 -376
$EndPAD
$PAD
Sh "53" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po -13250 -1126
$EndPAD
$PAD
Sh "54" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po -12750 -376
$EndPAD
$PAD
Sh "55" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po -12250 -1126
$EndPAD
$PAD
Sh "56" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po -11750 -376
$EndPAD
$PAD
Sh "57" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po -9250 -1126
$EndPAD
$PAD
Sh "58" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po -8750 -376
$EndPAD
$PAD
Sh "59" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po -8250 -1126
$EndPAD
$PAD
Sh "60" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po -7750 -376
$EndPAD
$PAD
Sh "61" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po -7250 -1126
$EndPAD
$PAD
Sh "62" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po -6750 -376
$EndPAD
$PAD
Sh "63" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po -6250 -1126
$EndPAD
$PAD
Sh "64" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po -5750 -376
$EndPAD
$PAD
Sh "65" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po -5250 -1126
$EndPAD
$PAD
Sh "66" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po -4750 -376
$EndPAD
$PAD
Sh "67" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po -4250 -1126
$EndPAD
$PAD
Sh "68" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po -3750 -376
$EndPAD
$PAD
Sh "69" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po -3250 -1126
$EndPAD
$PAD
Sh "70" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po -2750 -376
$EndPAD
$PAD
Sh "71" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po -2250 -1126
$EndPAD
$PAD
Sh "72" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po -1750 -376
$EndPAD
$PAD
Sh "73" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po 750 -1126
$EndPAD
$PAD
Sh "74" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po 1250 -376
$EndPAD
$PAD
Sh "75" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po 1750 -1126
$EndPAD
$PAD
Sh "76" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po 2250 -376
$EndPAD
$PAD
Sh "77" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po 2750 -1126
$EndPAD
$PAD
Sh "78" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po 3250 -376
$EndPAD
$PAD
Sh "79" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po 3750 -1126
$EndPAD
$PAD
Sh "80" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po 4250 -376
$EndPAD
$PAD
Sh "81" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po 4750 -1126
$EndPAD
$PAD
Sh "82" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po 5250 -376
$EndPAD
$PAD
Sh "83" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po 5750 -1126
$EndPAD
$PAD
Sh "84" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po 6250 -376
$EndPAD
$PAD
Sh "85" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po 6750 -1126
$EndPAD
$PAD
Sh "86" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po 7250 -376
$EndPAD
$PAD
Sh "87" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po 7750 -1126
$EndPAD
$PAD
Sh "88" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po 8250 -376
$EndPAD
$PAD
Sh "89" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po 8750 -1126
$EndPAD
$PAD
Sh "90" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po 9250 -376
$EndPAD
$PAD
Sh "91" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po 9750 -1126
$EndPAD
$PAD
Sh "92" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po 10250 -376
$EndPAD
$PAD
Sh "93" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po 10750 -1126
$EndPAD
$PAD
Sh "94" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po 11250 -376
$EndPAD
$PAD
Sh "95" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po 11750 -1126
$EndPAD
$PAD
Sh "96" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po 12250 -376
$EndPAD
$PAD
Sh "97" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po 12750 -1126
$EndPAD
$PAD
Sh "98" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po 13250 -376
$EndPAD
$PAD
Sh "99" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po 13750 -1126
$EndPAD
$PAD
Sh "100" C 512 512 0 0 0
Dr 315 0 0
At STD N 00C0FFFF
Ne 0 ""
Po 14250 -376
$EndPAD
$PAD
Sh "" C 984 984 0 0 0
Dr 984 0 0
At HOLE N 00E0FFFF
Ne 0 ""
Po -15497 0
$EndPAD
$PAD
Sh "" C 984 984 0 0 0
Dr 984 0 0
At HOLE N 00E0FFFF
Ne 0 ""
Po 15502 0
$EndPAD
$EndMODULE DIMM_100
$EndLIBRARY
