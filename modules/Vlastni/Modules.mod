PCBNEW-LibModule-V1  2.4.2012 19:03:31
# encoding utf-8
$INDEX
AXA012
LMX9838
ŐŚ.v€Í˘ţ˙˙˙Ú,vó-
$EndINDEX
$MODULE
Po 0 0 0 15 4F746F87 4F746F31 ~~
Li
Sc 4F746F31
AR
Op 0 0 0
T0 0 -5500 394 394 0 79 N V 21 N "AXA012"
T1 0 4000 394 394 0 79 N I 21 N "AXA012"
DS -4930 -5110 -4930 5120 110 21
DS -4930 5120 4920 5120 110 21
DS 4920 5120 4920 -5110 110 21
DS 4920 -5110 -4930 -5110 110 21
$PAD
Sh "1" R 580 580 0 0 0
Dr 380 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 2950 -1180
$EndPAD
$PAD
Sh "2" C 580 580 0 0 0
Dr 380 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 2950 -390
$EndPAD
$PAD
Sh "3" C 580 580 0 0 0
Dr 380 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 2950 400
$EndPAD
$PAD
Sh "4" C 580 580 0 0 0
Dr 380 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 2950 1190
$EndPAD
$PAD
Sh "5" C 580 580 0 0 0
Dr 380 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 2950 1970
$EndPAD
$PAD
Sh "6" C 580 580 0 0 0
Dr 380 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -2960 1970
$EndPAD
$PAD
Sh "7" C 580 580 0 0 0
Dr 380 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -2960 1190
$EndPAD
$PAD
Sh "8" C 580 580 0 0 0
Dr 380 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -2960 400
$EndPAD
$PAD
Sh "9" C 580 580 0 0 0
Dr 380 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -2960 -390
$EndPAD
$PAD
Sh "10" C 580 580 0 0 0
Dr 380 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -2960 -1180
$EndPAD
$PAD
Sh "11" C 580 580 0 0 0
Dr 380 0 0
At STD N 0CC08001
Ne 0 ""
Po -2960 -2750
$EndPAD
$EndMODULE
$MODULE LMX9838
Po 0 0 0 15 4F746FFA 00000000 ~~
Li LMX9838
Sc 00000000
AR
Op 0 0 0
T0 1969 -7087 394 394 0 79 N V 21 N "LMX9838"
T1 1969 -5906 394 394 0 79 N I 21 N "LMX9838"
DS 1651 3662 1961 3662 110 21
DS 1961 3662 1961 3432 110 21
DS -1659 3662 -1969 3662 110 21
DS -1969 3662 -1969 3432 110 21
DS -1969 -1528 -1969 -3028 110 21
DS -1969 -3028 1961 -3028 110 21
DS 1961 -3028 1961 -1608 110 21
DS -1189 -668 1171 -668 190 21
DS 1171 -668 1171 2882 190 21
DS 1171 2882 -1189 2882 190 21
DS -1189 2882 -1189 -668 190 21
$PAD
Sh "1" R 340 220 0 0 0
Dr 0 0 0
At SMD N 04888000
Ne 0 ""
Po -1779 -1178
$EndPAD
$PAD
Sh "2" R 340 220 0 0 0
Dr 0 0 0
At SMD N 04888000
Ne 0 ""
Po -1779 -778
$EndPAD
$PAD
Sh "3" R 340 220 0 0 0
Dr 0 0 0
At SMD N 04888000
Ne 0 ""
Po -1779 -388
$EndPAD
$PAD
Sh "4" R 340 220 0 0 0
Dr 0 0 0
At SMD N 04888000
Ne 0 ""
Po -1779 2
$EndPAD
$PAD
Sh "5" R 340 220 0 0 0
Dr 0 0 0
At SMD N 04888000
Ne 0 ""
Po -1779 402
$EndPAD
$PAD
Sh "6" R 340 220 0 0 0
Dr 0 0 0
At SMD N 04888000
Ne 0 ""
Po -1779 792
$EndPAD
$PAD
Sh "7" R 340 220 0 0 0
Dr 0 0 0
At SMD N 04888000
Ne 0 ""
Po -1779 1192
$EndPAD
$PAD
Sh "8" R 340 220 0 0 0
Dr 0 0 0
At SMD N 04888000
Ne 0 ""
Po -1779 1582
$EndPAD
$PAD
Sh "9" R 340 220 0 0 0
Dr 0 0 0
At SMD N 04888000
Ne 0 ""
Po -1779 1972
$EndPAD
$PAD
Sh "10" R 340 220 0 0 0
Dr 0 0 0
At SMD N 04888000
Ne 0 ""
Po -1779 2372
$EndPAD
$PAD
Sh "11" R 340 220 0 0 0
Dr 0 0 0
At SMD N 04888000
Ne 0 ""
Po -1779 2762
$EndPAD
$PAD
Sh "12" R 340 220 0 0 0
Dr 0 0 0
At SMD N 04888000
Ne 0 ""
Po -1779 3152
$EndPAD
$PAD
Sh "20" R 220 340 0 0 0
Dr 0 0 0
At SMD N 04888000
Ne 0 ""
Po 1371 3492
$EndPAD
$PAD
Sh "19" R 220 340 0 0 0
Dr 0 0 0
At SMD N 04888000
Ne 0 ""
Po 981 3492
$EndPAD
$PAD
Sh "18" R 220 340 0 0 0
Dr 0 0 0
At SMD N 04888000
Ne 0 ""
Po 581 3492
$EndPAD
$PAD
Sh "17" R 220 340 0 0 0
Dr 0 0 0
At SMD N 04888000
Ne 0 ""
Po 191 3492
$EndPAD
$PAD
Sh "16" R 220 340 0 0 0
Dr 0 0 0
At SMD N 04888000
Ne 0 ""
Po -199 3492
$EndPAD
$PAD
Sh "15" R 220 340 0 0 0
Dr 0 0 0
At SMD N 04888000
Ne 0 ""
Po -599 3492
$EndPAD
$PAD
Sh "14" R 220 340 0 0 0
Dr 0 0 0
At SMD N 04888000
Ne 0 ""
Po -989 3492
$EndPAD
$PAD
Sh "13" R 220 340 0 0 0
Dr 0 0 0
At SMD N 04888000
Ne 0 ""
Po -1379 3492
$EndPAD
$PAD
Sh "21" R 340 220 0 0 0
Dr 0 0 0
At SMD N 04888000
Ne 0 ""
Po 1761 3152
$EndPAD
$PAD
Sh "22" R 340 220 0 0 0
Dr 0 0 0
At SMD N 04888000
Ne 0 ""
Po 1761 2762
$EndPAD
$PAD
Sh "23" R 340 220 0 0 0
Dr 0 0 0
At SMD N 04888000
Ne 0 ""
Po 1761 2372
$EndPAD
$PAD
Sh "24" R 340 220 0 0 0
Dr 0 0 0
At SMD N 04888000
Ne 0 ""
Po 1761 1972
$EndPAD
$PAD
Sh "25" R 340 220 0 0 0
Dr 0 0 0
At SMD N 04888000
Ne 0 ""
Po 1761 1582
$EndPAD
$PAD
Sh "26" R 340 220 0 0 0
Dr 0 0 0
At SMD N 04888000
Ne 0 ""
Po 1761 1192
$EndPAD
$PAD
Sh "27" R 340 220 0 0 0
Dr 0 0 0
At SMD N 04888000
Ne 0 ""
Po 1761 792
$EndPAD
$PAD
Sh "28" R 340 220 0 0 0
Dr 0 0 0
At SMD N 04888000
Ne 0 ""
Po 1761 402
$EndPAD
$PAD
Sh "29" R 340 220 0 0 0
Dr 0 0 0
At SMD N 04888000
Ne 0 ""
Po 1761 2
$EndPAD
$PAD
Sh "30" R 340 220 0 0 0
Dr 0 0 0
At SMD N 04888000
Ne 0 ""
Po 1761 -388
$EndPAD
$PAD
Sh "31" R 340 220 0 0 0
Dr 0 0 0
At SMD N 04888000
Ne 0 ""
Po 1761 -778
$EndPAD
$PAD
Sh "32" R 340 220 0 0 0
Dr 0 0 0
At SMD N 04888000
Ne 0 ""
Po 1761 -1178
$EndPAD
$PAD
Sh "33" R 220 340 0 0 0
Dr 0 0 0
At SMD N 04888000
Ne 0 ""
Po 1371 -1118
$EndPAD
$PAD
Sh "34" R 220 340 0 0 0
Dr 0 0 0
At SMD N 04888000
Ne 0 ""
Po 981 -1118
$EndPAD
$PAD
Sh "35" R 220 340 0 0 0
Dr 0 0 0
At SMD N 04888000
Ne 0 ""
Po 581 -1118
$EndPAD
$PAD
Sh "36" R 220 340 0 0 0
Dr 0 0 0
At SMD N 04888000
Ne 0 ""
Po 191 -1118
$EndPAD
$PAD
Sh "37" R 220 340 0 0 0
Dr 0 0 0
At SMD N 04888000
Ne 0 ""
Po -199 -1118
$EndPAD
$PAD
Sh "38" R 220 340 0 0 0
Dr 0 0 0
At SMD N 04888000
Ne 0 ""
Po -599 -1118
$EndPAD
$PAD
Sh "39" R 220 340 0 0 0
Dr 0 0 0
At SMD N 04888000
Ne 0 ""
Po -989 -1118
$EndPAD
$PAD
Sh "40" R 220 340 0 0 0
Dr 0 0 0
At SMD N 04888000
Ne 0 ""
Po -1379 -1118
$EndPAD
$EndMODULE  LMX9838
$MODULE AXA012
Po 0 0 0 15 4F746F87 00000000 ~~
Li AXA012
Sc 00000000
AR 
Op 0 0 0
T0 0 -5500 394 394 0 79 N V 21 N "AXA012"
T1 0 4000 394 394 0 79 N I 21 N "AXA012"
DS -4930 -5110 -4930 5120 110 21
DS -4930 5120 4920 5120 110 21
DS 4920 5120 4920 -5110 110 21
DS 4920 -5110 -4930 -5110 110 21
$PAD
Sh "1" R 580 580 0 0 0
Dr 380 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 2950 -1180
$EndPAD
$PAD
Sh "2" C 580 580 0 0 0
Dr 380 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 2950 -390
$EndPAD
$PAD
Sh "3" C 580 580 0 0 0
Dr 380 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 2950 400
$EndPAD
$PAD
Sh "4" C 580 580 0 0 0
Dr 380 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 2950 1190
$EndPAD
$PAD
Sh "5" C 580 580 0 0 0
Dr 380 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 2950 1970
$EndPAD
$PAD
Sh "6" C 580 580 0 0 0
Dr 380 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -2960 1970
$EndPAD
$PAD
Sh "7" C 580 580 0 0 0
Dr 380 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -2960 1190
$EndPAD
$PAD
Sh "8" C 580 580 0 0 0
Dr 380 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -2960 400
$EndPAD
$PAD
Sh "9" C 580 580 0 0 0
Dr 380 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -2960 -390
$EndPAD
$PAD
Sh "10" C 580 580 0 0 0
Dr 380 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -2960 -1180
$EndPAD
$PAD
Sh "11" C 580 580 0 0 0
Dr 380 0 0
At STD N 0CC08001
Ne 0 ""
Po -2960 -2750
$EndPAD
$SHAPE3D
Na "Vlastni\\AXA012.wrl"
Sc 1.000000 1.000000 1.000000
Of 0.000000 0.000000 0.000000
Ro 0.000000 0.000000 0.000000
$EndSHAPE3D
$EndMODULE  AXA012
$EndLIBRARY
